package com.example.app.model.objective

import com.google.gson.annotations.SerializedName

class ObjectiveList(
    @SerializedName("portfolios")
    val objectives: List<Objective>
)