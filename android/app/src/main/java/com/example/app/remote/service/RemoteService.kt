package com.example.app.remote.service

import com.example.app.model.login.LoginResponse
import com.example.app.model.login.UserLogin
import com.example.app.model.objective.ObjectiveList
import com.example.app.remote.wrapper.resource.NetworkBoundResource
import com.example.app.remote.wrapper.resource.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RemoteService(
    private val api: RemoteAPI
) {
    suspend fun doLogin(userLogin: UserLogin): Flow<Resource<LoginResponse?>> {
        return flow {
            NetworkBoundResource(
                collector = this,
                processResponse = { it },
                call = api.postLoginAsync(userLogin)
            ).build()
        }
    }

    suspend fun getObjectives(accessToken: String): Flow<Resource<ObjectiveList?>> {
        return flow {
            NetworkBoundResource(
                collector = this,
                processResponse = { it },
                call = api.getObjectivesAsync(accessToken)
            ).build()
        }
    }
}