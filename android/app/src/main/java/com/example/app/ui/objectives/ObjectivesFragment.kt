package com.example.app.ui.objectives

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.app.R
import com.example.app.databinding.FragmentObjectivesBinding
import com.example.app.model.objective.Objective
import com.example.app.model.objective.Portfolio
import com.example.app.ui.dialog.ErrorDialog
import com.example.app.ui.dialog.LoaderDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.NumberFormat
import java.util.*

class ObjectivesFragment: Fragment() {

    private lateinit var binding: FragmentObjectivesBinding
    private lateinit var adapter: ObjectivesAdapter

    private val viewModel: ObjectivesViewModel by viewModel()

    private lateinit var loader: LoaderDialog
    private lateinit var errorDialog: ErrorDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)
        binding = FragmentObjectivesBinding.inflate(layoutInflater)
        loader = LoaderDialog()
        onInitValues()
        return binding.root
    }

    private fun showErrorDialog(
        title: String = "Oops",
        message: String = "Unknown error",
        action: () -> Unit = {}
    ) {
        errorDialog = ErrorDialog(message, title, action)
        errorDialog.show(childFragmentManager, "errorDialog")
    }

    private fun onInitValues() {
        loader.show(childFragmentManager, "loaderDialog")

        successConfiguration()
        errorConfiguration()

        binding.imageButtonClose.setOnClickListener {
            requireView().findNavController().navigate(R.id.action_objectives_to_login)
        }
    }

    private fun successConfiguration() {
        val objectivesObserver = Observer<List<Objective>> {
            adapter = ObjectivesAdapter(it, requireContext())
            binding.recyclerviewObjectives.adapter = adapter
            loader.dismiss()
        }

        viewModel.objectives.observe(requireActivity(), objectivesObserver)

        portfolioObserver()
    }

    private fun portfolioObserver() {
        val portfolioObserver = Observer<Portfolio> {
            val formatter = NumberFormat.getCurrencyInstance(Locale("pt", "br"))
            binding.textviewAmount.text = formatter.format(it.totalIncome)
            if (!it.hasObjectives) {
                binding.textviewWithoutItems.visibility = View.VISIBLE
            }
        }

        viewModel.portfolio.observe(viewLifecycleOwner, portfolioObserver)
    }

    private fun errorConfiguration() {
        val errorMessageObserver = Observer<String> {
            binding.textviewWithoutItems.visibility = View.VISIBLE
            showErrorDialog(message = it)
            loader.dismiss()
        }

        viewModel.errorMessage.observe(viewLifecycleOwner, errorMessageObserver)
    }

}
