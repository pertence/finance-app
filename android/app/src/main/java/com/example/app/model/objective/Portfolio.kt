package com.example.app.model.objective

data class Portfolio(
    val totalIncome: Double,
    val objectives: List<Objective>
) {
    val hasObjectives = objectives.isNotEmpty()
}