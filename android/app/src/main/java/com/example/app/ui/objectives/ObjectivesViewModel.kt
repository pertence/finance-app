package com.example.app.ui.objectives


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.model.objective.Objective
import com.example.app.model.objective.ObjectiveList
import com.example.app.model.objective.Portfolio
import com.example.app.persistence.PreferencesManager
import com.example.app.remote.service.RemoteService
import com.example.app.remote.wrapper.resource.Resource
import com.example.app.remote.wrapper.resource.Status
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ObjectivesViewModel(
    private val service: RemoteService,
    private val preferences: PreferencesManager,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _errorMessage: MutableLiveData<String> = MutableLiveData()

    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _portfolio = MutableLiveData<Portfolio>()

    val portfolio: LiveData<Portfolio>
        get() = _portfolio

    val objectives: LiveData<List<Objective>> by lazy {
        val objectives = MutableLiveData<List<Objective>>()

        viewModelScope.launch(dispatcher) {
            loadObjectives(objectives)
        }

        return@lazy objectives
    }

    private suspend fun loadObjectives(objectives: MutableLiveData<List<Objective>>) {
        service.getObjectives(preferences.accessToken ?: "").collect { resource ->
            if (hasObjectives(resource)) {
                objectives.postValue(resource.data?.objectives)
                setupPortfolio(resource)
            }

            if (requestFailed(resource)) {
                if (resource.message != null) {
                    _errorMessage.postValue(resource.message)
                    objectives.postValue(listOf())
                }
            }
        }
    }

    private fun setupPortfolio(resource: Resource<ObjectiveList?>) {
        var totalIncome = 0.0

        resource.data?.objectives?.forEach {
            totalIncome += it.totalBalance
        }

        _portfolio.postValue(
            Portfolio(
                totalIncome = totalIncome,
                objectives = resource.data?.objectives ?: listOf()
            )
        )
    }

    private fun requestFailed(resource: Resource<ObjectiveList?>) =
        resource.requestStatus == Status.Error

    private fun hasObjectives(resource: Resource<ObjectiveList?>) =
        resource.requestStatus == Status.Success && resource.data?.objectives != null
}
