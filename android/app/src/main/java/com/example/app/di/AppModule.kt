package com.example.app.di

import com.example.app.BuildConfig
import com.example.app.persistence.PreferencesManager
import com.example.app.remote.NetworkHandler
import com.example.app.remote.callAdapter.CallAdapterFactory
import com.example.app.remote.service.RemoteAPI
import com.example.app.remote.service.RemoteService
import com.example.app.ui.login.LoginViewModel
import com.example.app.ui.objectives.ObjectivesViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val serviceModule = module {
    single { RemoteService(get()) }
}

val networkModule = module {
    single { retrofit() }
    single { get<Retrofit>().create(RemoteAPI::class.java) }
}

private fun retrofit() = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create(NetworkHandler.gsonBuilder()))
    .baseUrl(BuildConfig.base_url)
    .addCallAdapterFactory(CallAdapterFactory())
    .client(NetworkHandler.httpClient())
    .build()

val preferencesModule = module {
    single { PreferencesManager(get()) }
}

val viewModelModule = module {
    viewModel { LoginViewModel(get(), get(), Dispatchers.IO) }
    viewModel { ObjectivesViewModel(get(), get(), Dispatchers.IO) }
}

val appModule =
    listOf(networkModule, serviceModule, viewModelModule, preferencesModule)
