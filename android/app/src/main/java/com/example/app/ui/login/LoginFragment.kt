package com.example.app.ui.login

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.app.R
import com.example.app.databinding.FragmentLoginBinding
import com.example.app.model.login.LoginResponse
import com.example.app.ui.dialog.LoaderDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment: Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var loader: LoaderDialog

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)
        binding = FragmentLoginBinding.inflate(layoutInflater)
        loader = LoaderDialog()
        onInitValues()
        return binding.root
    }

    private fun onInitValues() {
        binding.buttonLogin.setOnClickListener {
            loader.show(childFragmentManager, "loaderDialog")
            Handler(Looper.getMainLooper()).postDelayed( {
                loader.dismiss()
            }, 1000) // Loader auto dismiss after 1 second
            viewModel.login(
                binding.emailEditText.text.toString(),
                binding.passwordEditText.text.toString()
            )
        }
        loginObservableControl()
    }

    private fun loginObservableControl() {
        val loginObservable = Observer<LoginResponse> {
            loader.dismiss()
            requireView().findNavController().navigate(R.id.action_login_to_objectives)
        }
        viewModel.loginResponse.observe(viewLifecycleOwner, loginObservable)
    }
}