package com.example.app.remote.wrapper.resource

enum class Status {
    Loading,
    Success,
    Error
}