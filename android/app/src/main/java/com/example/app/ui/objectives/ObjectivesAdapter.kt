package com.example.app.ui.objectives


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.app.R
import com.example.app.databinding.RowObjectiveBinding
import com.example.app.model.objective.Objective
import java.text.NumberFormat
import java.util.*

class ObjectivesAdapter(
    private val items: List<Objective>,
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class ViewHolder(val binding: RowObjectiveBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            RowObjectiveBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val model = items[position]

        with(holder as ViewHolder) {
            val formatter = NumberFormat.getCurrencyInstance(Locale("pt", "br"))

            binding.textviewObjectiveName.text = model.name
            binding.textviewObjectiveAmount.text = formatter.format(model.totalBalance)

            Glide.with(context)
                .load(model.background.small)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .error(R.drawable.ic_baseline_broken_image_24)
                .into(binding.imageThumb)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

}
