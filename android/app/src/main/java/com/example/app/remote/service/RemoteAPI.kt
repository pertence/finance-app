package com.example.app.remote.service

import com.example.app.model.login.LoginResponse
import com.example.app.model.login.UserLogin
import com.example.app.model.objective.ObjectiveList
import com.example.app.remote.wrapper.ApiResult
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface RemoteAPI {

    @POST("account/login")
    fun postLoginAsync(
        @Body login: UserLogin
    ): Deferred<ApiResult<LoginResponse>>

    @GET("portfolios/mine")
    fun getObjectivesAsync(
        @Header("access-token") accessToken: String
    ): Deferred<ApiResult<ObjectiveList>>
}