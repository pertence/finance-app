package com.example.app.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.model.login.LoginResponse
import com.example.app.model.login.UserLogin
import com.example.app.persistence.PreferencesManager
import com.example.app.remote.service.RemoteService
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect

class LoginViewModel(
    private val service: RemoteService,
    private val preferences: PreferencesManager,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    val loginResponse = MutableLiveData<LoginResponse>()

    fun login(email: String, password: String) {
        val userLogin = UserLogin(email = email, password = password)

        viewModelScope.launch(dispatcher) {
            service.doLogin(userLogin).collect {
                preferences.accessToken = it.data?.accessToken
                if (it.data != null) {
                    loginResponse.postValue(it.data)
                }
            }
        }
    }
}