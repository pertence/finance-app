package com.example.app.remote.wrapper.resource

data class Resource<T>(
    val requestStatus: Status,
    val data: T? = null,
    val message: String? = null
) {
    companion object {

        fun <T> success(data: T?): Resource<T> = Resource(Status.Success, data, null)

        fun <T> error(message: String?, data: T? = null): Resource<T> =
            Resource(Status.Error, data, message)

        fun <T> loading(data: T? = null): Resource<T> = Resource(Status.Loading, data, null)

    }
}