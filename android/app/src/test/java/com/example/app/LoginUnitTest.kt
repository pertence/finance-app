package com.example.app

import com.example.app.di.networkModule
import com.example.app.di.serviceModule
import com.example.app.model.login.UserLogin
import com.example.app.remote.service.RemoteService
import com.example.app.remote.wrapper.resource.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject

class LoginUnitTest : KoinTest {

    private val service: RemoteService by inject()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(serviceModule, networkModule)
    }

    @Test
    fun login_success() {
        val userLogin = UserLogin(email = "mobile_test@warrenbrasil.com", password = "Warren123!")
        var status: Status? = null
        runBlocking {
            service.doLogin(userLogin).collect {
                status = it.requestStatus
            }
        }
        assertEquals(Status.Success, status)
    }

    @Test
    fun login_error() {
        val userLogin = UserLogin(email = "mobile_test@warrenbrasil.com", password = "War")
        var status: Status? = null
        runBlocking {
            service.doLogin(userLogin).collect {
                status = it.requestStatus
            }
        }
        assertEquals(Status.Error, status)
    }
}